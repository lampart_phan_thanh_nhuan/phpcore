<!doctype html>
<html lang="en">

<head>
    <title>Design Pattern</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        h1,
        h3,
        h4,
        h5 {
            text-align: center;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-sm navbar-light bg-light">
        <a class="navbar-brand" href="../index.html">Design Pattern</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavId">
             <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Parts</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item active" href="#">Part 1 - Việt Anh</a>
                        <a class="dropdown-item" href="../thanh_hai/01-AbstractFactory/main.php">Part 2 - Thanh Hai</a>
                    </div>
                </li>
                <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Creational Pattern</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item" href="../1.Factory-Pattern/main.php">1. Factory Pattern</a>
                        <a class="dropdown-item" href="../2.Singleton-Pattern/main.php">2. Singleton Pattern</a>
                        <a class="dropdown-item active" href="../3.Prototype-Pattern/main.php">3. Prototype Pattern</a>

                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Structural Pattern</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item" href="../4.Bridge-Pattern/main.php">1. Bridge Pattern</a>
                        <a class="dropdown-item" href="../5.Composite-Pattern/main.php">2. Composite Pattern</a>
                        <a class="dropdown-item" href="../6.Facade-Pattern/main.php">3. Facade Pattern</a>
                        <a class="dropdown-item" href="../7.Proxy-Pattern/main.php">4. Proxy Pattern</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Behavioral Pattern</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item" href="../8.Command-Pattern/main.php">1. Command Pattern</a>
                        <a class="dropdown-item" href="../9.Iterator-Pattern/main.php">2. Iterator Pattern</a>
                        <a class="dropdown-item" href="../10.Memento-Pattern/main.php">3. Memento Pattern</a>
                        <a class="dropdown-item" href="../11.State-Pattern/main.php">4. State Pattern</a>
                        <a class="dropdown-item" href="../12.Strategy-Pattern/main.php">5. Strategy Pattern</a>
                        <a class="dropdown-item" href="../13.Visitor-Pattern/main.php">6. Visitor Pattern</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Others Pattern</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item" href="../14.Delegate-Pattern/main.php">1. Business Delegate Pattern</a>
                        <a class="dropdown-item" href="../15.Data-Access-Object-Pattern/main.php">2. Data Access Object Pattern</a>
                        <a class="dropdown-item" href="../16.Intercepting-Filter-Pattern/main.php">3. Intercepting Filter Pattern</a>
                        <a class="dropdown-item" href="../17.Transfer-Object-Pattern/main.php">4. Transfer Object Pattern</a>
                        <a class="dropdown-item" href="../18.Repository-Pattern/main.php">5. Repository Pattern</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Diagram</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item" href="../DesignPattern.html">1. Full Diagram Design Pattern</a>
                        <a class="dropdown-item" href="../demo1.html">2. Diagram mô hình CPE</a>
                        <a class="dropdown-item" href="../demo2.html">3. Diagram mô hình CFSRPE</a>
                        <a class="dropdown-item" href="../demo3.html">4. Diagram mô hình CRCMD</a>
                    </div>
                </li>

            </ul>
        </div>
    </nav>
    <div class="container-fuild" style="padding: 50px;">
        <h3><strong>Prototype Pattern</strong></h3>
        <div class="row">
            <div class="col-sm-4">
                <h4>Định nghĩa</h4>
                <p>Pattern này cung cấp một trong những cách tốt nhất để tạo ra một đối tượng thay vì tạo ra Object, Prototype pattern sử dụng việc cloning (copy nguyên mẫu của Object). </p>
                <hr>
                <h4>Sử dụng khi nào ?</h4>
                <p>1. Khi việc tạo một object tốn nhiều chi phí và thời gian trong khi bạn đã có một object tương tự tồn tại.</p>
                <p>2. Khi copy từ object ban đầu sang object mới và thay đổi giá trị một số thuộc tính nếu cần</p>
                
                <hr>
                <h4>Kết quả demo</h4>
                <?php
                require_once "BookPrototype.php";
                require_once "PHPBookPrototype.php";
                require_once "SQLBookPrototype.php";
                require_once "HTMLBookPrototype.php";
                echo "BEGIN TESTING PROTOTYPE PATTERN <br>";

                $phpProto = new PHPBookPrototype();
                $sqlProto = new SQLBookPrototype();
                $htmlProto = new HTMLBookPrototype();

                $book1 = clone $sqlProto; //Nhân bản đối tượng book1 từ sqlProto
                $book1->setTitle("SQL for Cats");

                echo "Book 1 topic: " . $book1->getTopic() . "<br>";
                echo "Book 1 title: " . $book1->getTitle() . "<br>";

                $book2 = clone $phpProto;
                $book2->setTitle("Learning PHP from Scratch");

                echo "Book 2 topic: " . $book2->getTopic() . "<br>";
                echo "Book 2 title: " . $book2->getTitle() . "<br>";

                $book3 = clone $htmlProto;
                $book3->setTitle("Learning SQL from Scratch");

                echo "Book 3 topic: " . $book3->getTopic() . "<br>";
                echo "Book 3 title: " . $book3->getTitle() . "<br>";

                echo "END";
                ?>
            </div>
            <div class="col-sm-8">
                <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                        <li data-target="#demo" data-slide-to="2"></li>
                        <li data-target="#demo" data-slide-to="3"></li>
                    </ul>
                    <div class="carousel-inner">
                        <div class="carousel-item active"  style="vertical-align:middle; text-align:center">
                            <img src="image/minhhoa.PNG" alt="Hình minh họa" style="margin-top: 14%;">
                            <div class="carousel-caption" style=" top: 0;bottom: auto;">
                                <h2 style="color: black;"><strong>Hình minh họa</strong></h2>
                            </div>
                        </div>

                        <div class="carousel-item"  style="vertical-align:middle; text-align:center">
                            <img src="image/cautruc.PNG" alt="Cấu trúc" style="margin-top: 10%;">
                            <div class="carousel-caption" style=" top: 0;bottom: auto;">
                                <h2 style="color: black;"><strong>Cấu trúc</strong></h2>
                            </div>
                        </div>
                        <div class="carousel-item"  style="vertical-align:middle; text-align:center">
                            <img src="image/magia.PNG" alt="Mã giả" style="margin-top: 10%;">
                            <div class="carousel-caption" style=" top: 0;bottom: auto;">
                                <h2 style="color: black;"><strong>Mã giả</strong></h2>
                            </div>
                        </div>
                        <div class="carousel-item "  style="vertical-align:middle; text-align:center">
                            <img src="image/prototype.jpg" alt="UML" style="margin-top: 10%;">
                            <div class="carousel-caption" style=" top: 0;bottom: auto;">
                                <h2 style="color: black;"><strong>Mô hình UML</strong></h2>
                            </div>
                        </div>
                        <!-- Left and right controls -->
                        <a style="background-color: black;width: 5%;" class="carousel-control-prev" href="#demo" data-slide="prev">
                            <span style="color: black;" class="carousel-control-prev-icon"></span>
                        </a>
                        <a style="background-color: black;width: 5%;" class="carousel-control-next" href="#demo" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>