<?php
namespace Builder;

class Meal
{
    private $items = [];

    public function addItem(Item $item)
    {
        array_push($this->items,$item);
    }

    public function getCost(): float
    {
        $cost = 0;
        foreach ($this->items as &$item) {
            $cost += $item->price();
        }
        return $cost;
    }

    public function showItems(){
      foreach ($this->items as $item) {
         echo "Item : " . $item->name();
         echo "<br>";
         echo "Packing : " . $item->packing()->pack();
         echo "<br>";
         echo "Price : " . $item->price();
         echo "<br>";
      }
    }
}