<?php
namespace Builder;
require_once "Burger.php";

class  ChickenBurger extends Burger
{
    public function name(): string
    {
        return "Chicken Burger";
    }

    public function price(): float
    {
        return 50.5;
    }
}