<?php
namespace Builder;
require_once "ColdDrink.php";

class Coke extends ColdDrink
{
    public function name(): string
    {
        return "Coke";
    }

    public function price(): float
    {
        return 30.0;
    }
}