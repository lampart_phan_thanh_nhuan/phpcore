<?php

namespace Builder;
require_once("Meal.php");
require_once("VegBurger.php");
require_once("Coke.php");
require_once("ChickenBurger.php");
require_once("Pepsi.php");

class MealBuilder
{
    public function prepareVegMeal(): Meal
    {
        $meal = new Meal();
        $meal->addItem(new VegBurger());
        $meal->addItem(new Coke());
        return $meal;
    }

    public function prepareNonVegMeal(): Meal
    {
        $meal = new Meal();
        $meal->addItem(new ChickenBurger());
        $meal->addItem(new Pepsi());
        return $meal;
    }
}