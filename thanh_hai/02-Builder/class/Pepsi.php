<?php
namespace Builder;
require_once "ColdDrink.php";

class Pepsi extends ColdDrink
{
    public function name(): string
    {
        return "Pepsi";
    }

    public function price(): float
    {
        return 35.0;
    }
}