<?php

namespace ChainOfResponsibility;

abstract class AbstractLogger
{
    public static int $INFO = 1;
    public static int $DEBUG = 2;
    public static int $ERROR = 3;

    protected int $level;

    //next element in chain or responsibility
    protected $nextLogger;

    public function setNextLogger(AbstractLogger $nextLogger)
    {
        $this->nextLogger = $nextLogger;
    }

    public function logMessage(int $level, string $message)
    {
        if ($this->level <= $level) {
            $this->write($message);
        }
        if ($this->nextLogger != null) {
            $this->nextLogger->logMessage($level, $message);
        }
    }

    abstract protected function write(string $message);
}