<?php
namespace NullObjectPattern;
?>
<html lang="en">

<head>
    <title>Null Object Pattern</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        h1,
        h3,
        h4,
        h5 {
            text-align: center;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-expand-sm navbar-light bg-light">
    <a class="navbar-brand" href="../../index.html">Design Pattern</a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId"
            aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavId">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Parts</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item " href="../../1.Factory-Pattern/main.php">Part 1 - Việt Anh</a>
                        <a class="dropdown-item active" href="#">Part 2 - Thanh Hai</a>
                    </div>
                </li>
            <li class="nav-item active dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">Creational Pattern</a>
                <div class="dropdown-menu" aria-labelledby="dropdownId">
                    <a class="dropdown-item active" href="../01-AbstractFactory/main.php">1. AbstractFactory</a>
                    <a class="dropdown-item" href="../02-Builder/main.php">2. Builder Pattern</a>

                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">Structural Pattern</a>
                <div class="dropdown-menu" aria-labelledby="dropdownId">
                    <a class="dropdown-item" href="../03-Adapter/main.php">1. Adapter Pattern</a>
                    <a class="dropdown-item" href="../05-Decorator/main.php">2. Decocator Pattern</a>
                    <a class="dropdown-item" href="../06-Flyweight/main.php">3. Flyweight Pattern</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">Behavioral Pattern</a>
                <div class="dropdown-menu" aria-labelledby="dropdownId">
                    <a class="dropdown-item" href="../07-ChainsOfResponsibility/main.php">1. Chain Of Responsibility
                        Pattern</a>
                    <a class="dropdown-item" href="../08-Interpreter/main.php">2. Iterpreter Pattern</a>
                    <a class="dropdown-item" href="../09-Mediator/main.php">3. Mediator Paffttern</a>
                    <a class="dropdown-item" href="../10-Observer/main.php">4. Observer Pattern</a>
                    <a class="dropdown-item" href="../12-Template/main.php">5. Template Pattern</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">Others</a>
                <div class="dropdown-menu" aria-labelledby="dropdownId">
                    <a class="dropdown-item" href="../04-Filter/main.php">1. Filter</a>
                    <a class="dropdown-item" href="../11-NullObjectPattern/main.php">2. Null Object Pattern</a>
                    <a class="dropdown-item" href="../13-MVC/main.php">3. MVC Pattern</a>
                    <a class="dropdown-item" href="../14-CompositeEntity/main.php">4. Composite Entity Pattern</a>
                    <a class="dropdown-item" href="../15-FrontController/main.php">5. Front Controller Pattern</a>
                    <a class="dropdown-item" href="../16-ServiceLocator/main.php">6. Service Locator Pattern</a>
                </div>
            </li>

            <a class="nav-link" href="../DesignPattern.html">Full Diagram</a>

        </ul>
    </div>
</nav>
<div class="container-fuild" style="padding: 50px;">
    <h3><strong>Null Object Pattern</strong></h3>
    <hr>
    <div class="row">
        <div class="col-sm-4">
            <h4>Định nghĩa</h4>
            <p>Null Object Pattern là một trong những Pattern thuộc nhóm hành vi (Behavior Pattern).
                Null Object pattern không phải là một Gang of Four Design Pattern
                Tư tưởng của Null Object là sử dụng một đối tượng Null đặc biệt để gói gọn sự vắng mặt của một thể hiện
                bằng cách cung cấp một sự thay thế hành xử theo cách thụ động phù hợp
            </p>
            <hr>
            <h4>Sử dụng khi nào ?</h4>
            <p>
                Đối phó với các đối tượng null.
                Thay vì kiểm tra đối tượng null, chúng ta xác định hành vi null hoặc hành vi được gọi nhưng không làm gì.
                Cung cấp hành vi, giá trị mặc định trong trường hợp dữ liệu không có sẵn.
                Tạo các đối tượng để thử nghiệm, trong trường hợp tài nguyên thật không có sẵn.
            </p>

            <hr>
            <h4>Kết quả demo</h4>
            <?php


            require_once("class/AbstractCustomer.php");
            require_once("class/CustomerFactory.php");
            require_once("class/NullCustomer.php");
            require_once("class/RealCustomer.php");


            $customer1 = CustomerFactory::getCustomer("Rob");
            $customer2 = CustomerFactory::getCustomer("Bob");
            $customer3 = CustomerFactory::getCustomer("Julie");
            $customer4 = CustomerFactory::getCustomer("Laura");

            echo "Customers";
            echo "<br>";
            echo $customer1->getName();
            echo "<br>";
            echo $customer2->getName();
            echo "<br>";
            echo $customer3->getName();
            echo "<br>";
            echo $customer4->getName();

            ?>

            <h2>
                Intent
            </h2>
            <h3>
                The intent of a Null Object is to encapsulate the absence of an object by providing a substitutable
                alternative that offers suitable default do nothing behavior. In short, a design where "nothing will
                come of nothing"
            </h3>
            <h3>
                Use the Null Object pattern when
            </h3>
            <h3>
                an object requires a collaborator. The Null Object pattern does not introduce this collaboration--it
                makes use of a collaboration that already exists
                some collaborator instances should do nothing
            </h3>
            <h3>
                you want to abstract the handling of null away from the client
            </h3>

            <h2>
                Ví dụ
            </h2>

            <h2>
                //Thông thường chúng ta gán 1 đối tượng tham chiếu đến giá trị null để thể hiện việc chưa có giữ liệu
            </h2>
            <h3>
                $customer = null;
            </h3>
            <h3>
                $customer->getName(); // null pointer exception
            </h3>

            <h2>
                // áp dụng design pattern Null Object
            </h2>
            <h3>
                $customer = new NullCustomer();
            </h3>
            <h3>
                $customer->getName(); // do nothing
            </h3>


        </div>
        <div class="col-sm-8">
            <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false">

                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <!--                    <li data-target="#demo" data-slide-to="1"></li>-->
                    <!--                    <li data-target="#demo" data-slide-to="2"></li>-->
                    <!--                    <li data-target="#demo" data-slide-to="3"></li>-->
                </ul>

                <!-- The slideshow -->
                <div class="carousel-inner">
                    <!--                    <div class="carousel-item active">-->
                    <!--                        <img src="image/minhhoa.PNG" alt="Hình minh họa" style="margin-left: 10%;margin-top: 14%;">-->
                    <!--                        <div class="carousel-caption" style=" top: 0;bottom: auto;">-->
                    <!--                            <h2 style="color: black;"><strong>Hình minh họa</strong></h2>-->
                    <!--                        </div>-->
                    <!--                    </div>-->

                    <div class="carousel-item active">
                        <img src="image/null_pattern.jpg" alt="UML" style="margin-left: 10%;margin-top: 10%;">
                        <div class="carousel-caption" style=" top: 0;bottom: auto;">
                            <h2 style="color: black;"><strong>Mô hình UML</strong></h2>
                        </div>
                    </div>

                </div>

                <!-- Left and right controls -->
                <a style="background-color: black;width: 5%;" class="carousel-control-prev" href="#demo"
                   data-slide="prev">
                    <span style="color: black;" class="carousel-control-prev-icon"></span>
                </a>
                <a style="background-color: black;width: 5%;" class="carousel-control-next" href="#demo"
                   data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>

            </div>


        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>