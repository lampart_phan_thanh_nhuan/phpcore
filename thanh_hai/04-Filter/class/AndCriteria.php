<?php

namespace Filter;


class AndCriteria implements Criteria
{
    private Criteria $criteria;
    private Criteria $otherCriteria;

    public function __construct(Criteria $criteria, Criteria $otherCriteria) {
      $this->criteria = $criteria;
      $this->otherCriteria = $otherCriteria;
   }

    public function meetCriteria($persons): array
    {
        $firstCriteriaPersons = $this->criteria->meetCriteria($persons);
      return $this->otherCriteria->meetCriteria($firstCriteriaPersons);
    }

}
