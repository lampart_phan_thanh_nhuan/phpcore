<?php

namespace ObserverPartern;

abstract class Observer
{
    protected Subject $subject;
    public abstract function update();
}